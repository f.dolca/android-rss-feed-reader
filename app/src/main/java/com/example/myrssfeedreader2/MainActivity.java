package com.example.myrssfeedreader2;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.example.myrssfeedreader2.R.id.btnCinemaBlend;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    ArrayList<String> rssLinks = new ArrayList<>();
    private int i;
    private Map<Integer, String> bookmarksLinks = new HashMap();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnRediff = findViewById(R.id.btnRediff);
        Button btnCinemaBlend = findViewById(R.id.btnCinemaBlend);
        Button btnSearch = findViewById(R.id.btnSearch);
        Button btnBookmark = findViewById(R.id.btnBookmark);

        btnRediff.setOnClickListener(this);
        btnCinemaBlend.setOnClickListener(this);
        btnSearch.setOnClickListener(this);
        btnBookmark.setOnClickListener(this);


        rssLinks.add("https://www.rediff.com/rss/moviesreviewsrss.xml");
        rssLinks.add("https://www.cinemablend.com/rss_review.php");
    }

    @Override
    public void onClick(View view) {
        EditText txtSearch = findViewById(R.id.txtSearch);
        switch (view.getId()) {
            case R.id.btnRediff:
                startActivity(new Intent(MainActivity.this, RSSFeedActivity.class).putExtra("rssLink", rssLinks.get(0)));
                break;
            case btnCinemaBlend:
                startActivity(new Intent(MainActivity.this, RSSFeedActivity.class).putExtra("rssLink", rssLinks.get(1)));
                break;
            case R.id.btnSearch:

                if (txtSearch.getText() != null && txtSearch.getText().toString().length() > 0) {
                    startActivity(new Intent(MainActivity.this, RSSFeedActivity.class).putExtra("rssLink", txtSearch.getText().toString()));
                }
                break;
            case R.id.btnBookmark:

                Button btn = new Button(this);
                btn.setId(i + 1);
                btn.setText(txtSearch.getText().toString());
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                btn.setLayoutParams(lp);
                bookmarksLinks.put(i + 1, txtSearch.getText().toString());

                btn.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        System.out.println(v.getId());

                        System.out.println(bookmarksLinks.get(v.getId()));
                        startActivity(new Intent(MainActivity.this, RSSFeedActivity.class).putExtra("rssLink", bookmarksLinks.get(i)));
                    }
                });
                ((LinearLayout) findViewById(R.id.mainLayoutView)).addView(btn);
                i++;
        }
    }
}

